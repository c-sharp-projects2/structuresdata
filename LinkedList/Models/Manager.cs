﻿namespace LinkedList.Models;

public class Manager<T>
{
    public Manager()
    {
        Head = null;
        Tail = null;
        Count = 0;
    }

    public Manager(T data)
    {
        Add(data);
    }

    public Node<T>? Head { get; set; }
    
    public Node<T>? Tail { get; set; }
    
    public int Count { get; set; }

    public T this[int index]
    {
        get
        {
            var node = ElementAt(index);
            if (node == null)
            {
                throw new IndexOutOfRangeException();
            }

            return node;
        }
    }

    public void Add(T data)
    {
        var node = new Node<T>(data);
        switch (Count)
        {
            case 0:
                Head = node;
                Tail = node;
                break;
            default:
                node.Previous = Tail;
                Tail!.Next = node;
                Tail = node;
                break;
        }

        Count++;
    }

    public void Insert(int index, T value)
    {
        var node = new Node<T>(value);

        if (index == 0)
        {
            if (Count == 0)
            {
                Add(value);
            }
            else
            {
                node.Next = Head;
                Head!.Previous = node;
                Head = node;
            }
        }
        else
        {
            var curNode = GetNode(index);
            if (curNode != null)
            {
                var prevNode = curNode.Previous;
                prevNode!.Next = node;
                node.Previous = prevNode;
                node.Next = curNode;
                curNode.Previous = node;
            }
        }

        Count++;
    }

    public void Remove(T value)
    {
        var nodeForDelete = GetNode(value);
        if (nodeForDelete != null)
        {
            if (nodeForDelete.Equals(Head))
            {
                Head!.Next!.Previous = null;
                Head = Head.Next;
            }
            else if (nodeForDelete.Equals(Tail))
            {
                Tail!.Previous!.Next = null;
                Tail = Tail.Previous;
            }
            else
            {
                var prevNode = nodeForDelete.Previous;
                var nextNode = nodeForDelete.Next;
                prevNode!.Next = nextNode;
                nextNode!.Previous = prevNode;
            }
        }

        Count--;
    }

    public int IndexOf(T value)
    {
        var index = 0;
        var fined = false;
        var current = Head;
        if (current != null)
        {
            while (current.Next != null)
            {
                if (current!.Data!.Equals(value))
                {
                    fined = true;
                }

                index++;
                current = current.Next;
            }
        }

        if (!fined)
        {
            index = -1;
        }

        return index;
    }

    public T? ElementAt(int index)
    {
        var item = default(T);
        var current = Head;
        if (current != null)
        {
            var curIndex = 0;
            while (current.Next != null && curIndex <= index)
            {
                item = current.Data;
                current = current.Next;
            }
        }

        return item;
    }

    private Node<T>? GetNode(int index)
    {
        Node<T>? node = null;
        var current = Head;
        var pos = 0;
        while (current?.Next != null && pos++ <= index)
        {
            node = current;
            current = current.Next;
        }

        return node;
    }

    private Node<T>? GetNode(T value)
    {
        Node<T>? node = null;
        var current = Head;
        while (current?.Next != null)
        {
            if (current!.Data!.Equals(value))
            {
                node = current;
                break;
            }

            current = current.Next;
        }

        return node;
    }
}