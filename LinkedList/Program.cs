﻿using LinkedList.Models;

var manager = new Manager<int>();

manager.Add(5);
manager.Add(15);
manager.Add(1);
manager.Add(4);
manager.Add(10);

manager.Remove(1);

manager.Insert(0, 123);
manager.Remove(10);

manager.Insert(2, 150);
