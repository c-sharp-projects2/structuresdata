﻿namespace SingleLinkedList.Models;

using System.Collections;

public class Manager<T> : IEnumerable<Node<T>>, IEnumerator<Node<T>>
{
    private Node<T>? head;
    private Node<T>? tail;

    private int count;

    public Manager(Node<T>? begin)
    {
        head = begin;
        tail = begin;
        count = 1;
    }

    public Manager(T data)
    {
        Add(data);
        count = 1;
    }

    public Manager()
    {
        head = null;
        tail = null;
    }

    public Node<T>? Current { get; private set; }

    public T this[int index]
    {
        get
        {
            var item = GetItem(index);
            if (item == null)
            {
                throw new IndexOutOfRangeException();
            }

            return item;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public IEnumerator<Node<T>> GetEnumerator()
    {
        return this;
    }

    public bool MoveNext()
    {
        var result = false;
        if (Current == null && head != null)
        {
            Current = head;
            result = true;
        }
        else if (Current?.Next != null)
        {
            Current = Current.Next;
            result = true;
        }
        
        return result;
    }

    public void Reset()
    {
        Current = null;
    }

    object? IEnumerator.Current => Current;

    public void Dispose()
    {
        GC.SuppressFinalize(this);
    }

    public void Add(T data)
    {
        var node = new Node<T>(data);
        switch (count)
        {
            case 0:
                head = node;
                tail = node;
                break;
            case 1:
                head!.Next = node;
                tail = node;
                break;
            default:
                tail!.Next = node;
                tail = node;
                break;
        }

        count++;
    }

    public void Insert(int index, T data)
    {
        var node = new Node<T>(data);
        if (index == 0)
        {
            node.Next = head;
            head = node;
        }
        else
        {
            var curNode = GetNode(index);
            if (curNode != null)
            {
                var prevNode = GetNode(index - 1);
                node.Next = curNode;
                prevNode!.Next = node;
            }
        }
    }

    public T GetItem(int index)
    {
        T data = default!;
        Reset();
        var position = 0;

        while (MoveNext() && position++ < index)
        {
            data = Current.Data;
        }
        
        return data;
    }


    public int IndexOf(T data)
    {
        var index = 0;
        var fined = false;
        Reset();

        while (MoveNext())
        {
            if (!Current!.Data!.Equals(data))
            {
                index++;
            }
            else
            {
                fined = true;
                break;
            }
        }

        if (!fined)
        {
            index = -1;
        }
        
        return index;
    }

    public void Remove(T data)
    {
        var node = GetNode(data);
        if (node != null)
        {
            if (node.Equals(head))
            {
                head = head.Next;
            }
            else
            {
                var curIndex = IndexOf(node.Data);
                var prevNode = GetNode(curIndex - 1);
                prevNode!.Next = node.Next;
            }
        }

        Reset();
        count--;
    }

    private Node<T>? GetNode(T data)
    {
        Node<T>? node = null;
        Reset();
        
        while (MoveNext())
        {
            if (Current!.Data!.Equals(data))
            {
                node = Current;
            }
        }
        
        return node;
    }

    private Node<T>? GetNode(int index)
    {
        Node<T>? node = null;
        Reset();
        var position = 0;

        while (MoveNext() && position++ <= index)
        {
            node = Current;
        }
        
        return node;
    }
}