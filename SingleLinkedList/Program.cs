﻿using SingleLinkedList.Models;

var a = new Manager<int>();

a.Add(1);
a.Add(10);
a.Add(15);
a.Add(20);
a.Add(31);
a.Add(42);

a.Insert(0, 2);
a.Insert(3, 3);

a.Remove(1);
a.Remove(20);
a.Remove(42);

Console.ReadKey();